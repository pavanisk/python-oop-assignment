from abc import ABC, abstractmethod

class Product:
    def __init__(self, name, description, price, sku):
        self.name = name
        self.description = description
        self.price = price
        self.sku = sku

class Order:
    def __init__(self, order_id, customer, shipper):
        self.order_id = order_id
        self.customer = customer
        self.shipper = shipper
        self.products = []
        self.total = 0
        self.status = "pending"

    def add_product(self, product, quantity):
        self.products.append((product, quantity))
        self.total += product.price * quantity

    def remove_product(self, product, quantity):
        if (product, quantity) in self.products:
            self.products.remove((product, quantity))
            self.total -= product.price * quantity

    def calculate_total(self):
        return self.total

class Customer:
    def __init__(self, customer_id, name, email, address):
        self.customer_id = customer_id
        self.name = name
        self.email = email
        self.address = address

class Shipper:
    def __init__(self, shipper_id, name, contact_info):
        self.shipper_id = shipper_id
        self.name = name
        self.contact_info = contact_info

class IConfirmation:
    @abstractmethod
    def send_confirmation(self):
        pass

class EmailConfirmation(IConfirmation):
    def send_confirmation(self, customer, order):
        # Send email confirmation to customer
        print(f"Email confirmation sent to {customer.email} for order {order.order_id}")

class ReceiptConfirmation(IConfirmation):
    def send_confirmation(self, customer, order):
        # Print receipt for customer
        print(f"Receipt printed for {customer.name} for order {order.order_id}")

class OrderProcessor:
    def __init__(self, confirmation):
        self.confirmation = confirmation

    def process_order(self, order):
        # Process the order
        order.status = "processed"

        # Send confirmation to customer
        self.confirmation.send_confirmation(order.customer, order)
